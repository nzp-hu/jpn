# Új szavak, kifejezések
| kanji | kana   | magyar    |
| ----- | ------ | --------- |
| 体験    | たいけん   | élmény    |
| 印象    | いんしょう  | benyomás  |
| 裸     | はだか    | mesztelen |
| 交換する  | こうかんする | cserélni  |
| 語彙    | ごい     | szókincs  |

*élménybeszámolós 会話 óra*
