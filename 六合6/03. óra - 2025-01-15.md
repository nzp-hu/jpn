# Új szavak, kifejezések
| kanji | kana | magyar         |
| ----- | ---- | -------------- |
| 模様    | もよう  | minta, pattern |

# Házi / 宿題
- első doc 23. dia
	1. もう　だれも　いないようですね。
	2. 強い風が吹いたようですね。
	3. 雨が降っているようですね。
	4. 留守のようですね。
- első doc 24. dia
	1. 子供の声がちょっとうるさいですね。
	   子供たちがけんかしているようですね。
	2. いい　におい　が　します。
	   ケーキを焼いているようですね。
	3. 変な味がします。
	   しょうゆ　と　ソース　を　まちがえたようです。

# PDF
![[./assets/pdf/03_ora.pdf]]