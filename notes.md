# Szótárak
- https://jisho.org
- http://adys.org

---

# Gyakorlás
- https://kana-reading-practice.glitch.me/
- https://tadoku.org/japanese/en/free-books-en/
- https://japanesetest4you.com/
- https://yomujp.com/
- https://www.irasutoya.com/

---

# Egyéb
- Kölcsönzött szavak: https://en.wikipedia.org/wiki/List_of_gairaigo_and_wasei-eigo_terms