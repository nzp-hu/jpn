# Új szavak, kifejezések

| romaji     | magyar              |
| ---------- | ------------------- |
| ryoukou    | kirándulni/elutazni |
| aji        | íz                  |
| kowai      | ijesztő, félelmetes |
| urusai     | hangos              |
| muzukashii | nehéz               |
| tsumetai   | hideg               |
| minikui    | ronda, nem szép     |
| haiiro     | szürke (szín)       |
| hato       | galamb              |

# Ok-okozat
Kérdőszó:

| romaji           | magyar |
| ---------------- | ------ |
| doushite         | miért? |
| doushite desuka? | miért? |

Kötőszó: ==kara==

Például:
- jikan ga arimasen ==kara==, tenisu wo shimasen - nincs időm, ezért nem teniszezek
- okane ga arimasen ==kara== - mert nincsen pénzem
- nihon ryouri wa oishii _desu_ ==kara==, gyoza ga daisuki - a japán ételek finomak, ezért nagyon szeretem a gyozát (a _desu_ nehogy lemaradjon!)

# Ellentétek
Kötőszava: ==ga==

Például:
- yasai ==wa== tabemasu ==ga==, niku wa tabemasen - zöldséget eszek, de húst nem
- sushi ==wa== suki desu ==ga==, niku wa kirai desu - szeretem a sushit, de nem szeretem a húst

# Házi / Shukudai
- Harmadik jamboard 7. dia, [PDF 3. dia](./assets/pdf/09_ora.pdf#page=3): kitölteni a párbeszédet
	1. jikan ga arimasen kara (jikan ga mo osoi desu kara?)
	2. doushite desuka? asagohan wo zenzen tabemasen kara.
	3. doushite desuka? kinou shoppingumooru ni ikimashita kara.

---

# PDF
![[./assets/pdf/09_ora.pdf]]