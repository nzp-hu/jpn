# Új szavak, kifejezések

| romaji    | magyar  |
| --------- | ------- |
| sanposuru | sétálni |

# "Valamit csinálni megyek"
Képzése:
> ige (levágva róla ます) + に + másik ige

Például:
- ごはん　を　たべに　いきます
- ふく　を　かいに　いきます
- えいが　を　みに　いきます

# Meghívás, invitálás, cselekvésre buzdítás
Segéd kifejezései:
> ～ませんか
> ～ましょう

A ～ませんか forma jelenthet meghívást, invitálást. A tagadó kérdő alak használata a beszélgető partnerre való nyomásgyakorlás elkerülését célozza.

A ～ましょう forma cselekvésre való felhívást fogalmaz meg, amelyet többes szám első személyű felszólító alakban fordítunk magyarra.

# Házi / Shukudai
- Második jamboard 8. dia, [PDF 7. dia](./assets/pdf/04_ora.pdf#page=7) bal oldal: igéket átalakítani a szótári alakra
	1. iku
	2. kaeru
	3. kuru
	4. kiku
	5. neru
	6. yomu
	7. sanposuru
	8. aru
	9. iru
- Második jamboard 8. dia, [PDF 7. dia](./assets/pdf/04_ora.pdf#page=7) jobb oldal: példamondatok alapján párbeszédet írni
	1. kayoubi ni kissaten de koohii wo nomimasenka? iidesune. nomimashou!
	2. suiyoubi ni isshoni tenisu shimasenka? tenisu desuka? chotto...
	3. mokuyoubi ni isshoni eiga wo mimasenka?  eiga desuka? iidesune. mimashou!
	4. getsuyoubi ni McD de tabemasenka? McD desuka? chotto...
	5. kinyoubi ni ocha wo nomimasenka? kinyoubi desuka? sumimasen.
	6. doyoubi ni aisukuriimu wo tabemasenka? iidesune. tabemashou!

---

# PDF
![[./assets/pdf/05_ora.pdf]]
