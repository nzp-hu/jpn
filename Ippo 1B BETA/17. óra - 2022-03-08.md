# Új szavak, kifejezések

| romaji         | magyar                                                      |
| -------------- | ----------------------------------------------------------- |
| migaku         | súrolni, pucolni                                            |
| dasu           | kienged/kifelé- - sok jelentése van szókapcsolattól függően |
| tegami wo dasu | levelet feladni                                             |
| tsukau         | használni                                                   |
| naku           | sírni, síró hangot kiadni                                   |

# Házi / Shukudai
- Ötödik jamboard 19. dia: utolsó link
- [Hatodik jamboard](https://jamboard.google.com/d/1IBIFDI0brjmgXeOcgfkWmSR0Jv-4Zcqrl_Lqxnyfl7o/viewer?f=2) 3. dia felső
	1. tomodachi no uchi ni itte, isshoni eiga wo mimasu
	2. uchi ni kaette, bangohan wo tabemasu
	3. densha wo orite, uchi he sanpo shimasu
	4. tomodachi ni atte, hambaaga wo tabemasu
	5. jitensha wo karite, kyousou (verseny?) shimasu
	6. daigaku ni itte, nanimo benkyou shimasen. :(
- [Hatodik jamboard](https://jamboard.google.com/d/1IBIFDI0brjmgXeOcgfkWmSR0Jv-4Zcqrl_Lqxnyfl7o/viewer?f=2) 3. dia alsó szorgalmi

---

# PDF
![[./assets/pdf/17_ora.pdf]]