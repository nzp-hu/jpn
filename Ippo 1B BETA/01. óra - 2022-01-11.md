# Új szavak, kifejezések

| romaji  | magyar            |
| ------- | ----------------- |
| osusume | ajánlani, ajánlás |
| erande  | kiválasztani      |

---

# PDF
![[./assets/pdf/01_ora.pdf]]