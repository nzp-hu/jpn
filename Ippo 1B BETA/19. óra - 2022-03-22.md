# Új szavak, kifejezések

| romaji       | magyar      |
| ------------ | ----------- |
| kaesu        | visszaadni  |
| wasureru     | elfelejteni |
| shinpai suru | aggódni     |
| semai        | keskeny     |
| sakubun      | fogalmazás  |
| kozutsumi    | csomag      |
| koukuubin    | légiposta   |
| funabin      | hajóposta   |
| kokunai      | belföldi    |
| zenbu        | összesen    |

# Házi / Shukudai
- **Rövid fogalmazást írni:** bármilyen téma, próbáljunk meg belerakni minden nyelvtant, amit vettünk az elmúlt hetekben. Nem feltétlen muszáj, hogy értelme legyen. Beküldeni fb csoportba

Amiket vettünk:
- [x] melléknév állítás, tagadás + múlt idő
- [x] felsorolás (て-forma)
- [x] okozat (て-forma)
- [x] -masenka (cselekvésre hívás)
- [x] -mashou
- [x] -mashouka
- [x] -tai desu
- [x] -tain desu ga, ...
- [x] "valamit csinálni menni"
- [x] tárgyrag (wo)
- [x] helyhatározó (de)
- [x] -ba/be (ni)
- [x] ellentét kifejezése (ga)
- [x] ok, okozat (kara)
- [x] dátum, idő?
- [x] napszak
- [ ] időjárás?

**Vázlat**
> - Milyen volt a japán óra?
> - Érdekes és "szórakoztató" volt, viszont kicsit fárasztó
> - Mikor van órád?
> - Kedd és csütörtök, x-től x-ig
> - Este? Nem késő az?
> - Nem, végzek a munkával, iszok egy kávét és kezdődik is az óra.
> - Hű. Én is szeretnék japánt tanulni, de túl elfoglalt vagyok mostanában.
> - Online az óra, úgyhogy otthonról be tudsz csatlakozni/otthonról tudsz tanulni.
> - Remek! Utána fogok nézni.
> - Más téma: szeretnék elmenni megnézni az új Dűne filmet; nem nézzük meg együtt?
> - Az nem is új, már 38 éves.
> - De ez egy remake.
> - Óh, akkor menjünk. Csak előbb befejezem ezt a fogalmazást
> - Segítsek?

> にほんご　の　じゅぎょ　は　どうでしたか。
> おもしろくて、たのしくて　でした　が、ちょっと　ぐだぐだしい　でした。
> じゅぎょ　は　いつ　ですか。
> かようび　と　もくようび　です。17:15から　18:45まで　です。
> よる　ですか。おそくない　ですか。
> いえ、しごと　を　とまて、コーヒー　を　のんで、そして　じゅぎょ　は　はじめます。
> わたし　も　にほんご　を　べんきょうしたいん　です　が、いそがしい　です。
> じゅぎょ　は　オンラインで　から、うち　で　べんきょう　する。
> すごい！　じゃあ　チェック　します。
> ところで、あたらしい　デューン えいが　を　みに　いきたい　です。いっしょに　いきませんか。
> それ　は　あたらしくない　です。38さい　です。その　えいが　は　すでに　38さい　です。
> これ　は　レメーク　です。
> え？　それでは　いきましょう。いく　まえ　に、この　さくぶん　を　しあげます。
> てつだい　ましょうか。

[Japanese conversation openers, fillers](https://www.thoughtco.com/japanese-conversation-openers-fillers-4077284)

**Fájlok**
**ODT:** [[さくぶん_-_ネヘル_ゾリ.odt]]
**PDF:**  [[さくぶん_-_ネヘル_ゾリ.pdf]]


---

# PDF
![[./assets/pdf/19_ora.pdf]]