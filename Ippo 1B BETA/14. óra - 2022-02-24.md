# Új szavak, kifejezések

| romaji       | magyar                                |
| ------------ | ------------------------------------- |
| ooi          | sok                                   |
| sukunai      | kevés                                 |
| kitanai      | rendezetlen, koszos                   |
| kata         | ember (udvarias változata a hito-nak) |
| oukyuu       | palota, vár                           |
| mushiatsui   | "nyálkás" párás fülledt meleg         |
| jimejime     | párás fülledt meleg                   |
| furu         | esni (csapadék)                       |
| saku         | virágzik, kinyílni                    |
| semi         | kabóca                                |
| semi ga naku | ciripel a kabóca                      |
| ajisai       | hortenzia virág                       |
| tsuyu        | esős évszak                           |
| uki          | esős időszak                          |
| kanki        | száraz időszak                        |
| ichinenjuu   | egész éves                            |
| shiki        | négy évszak                           |
| hiroba       | tér                                   |

# Házi
- [Ötödik jamboard](https://jamboard.google.com/d/1n1s3epY_tHnFnmNLyqf7e8VHmNGQz4CuJM88xV4B4YU/viewer) 1. dia, [PDF 4. dia](./assets/pdf/14_ora.pdf#page=4)
	- doushitandesuka
	- ~~ikitaidesuka~~ ikimasuka
	- ikitaindesuga
	- mazu
	- norimasu
	- soshite
	- sorekara
	- ikimashouka

---

# PDF
![[./assets/pdf/14_ora.pdf]]