- 1kg marhahús
- 30dkg vöröshagyma
- 5ek olaj
- só, bors
- 1ek pirospaprika
- 1tk kömény
- 3 gerezd fokhagyma
- 1db zöldpaprika
- 1 csip kakukkfű
- 1 csip majoránna
- 1db paradicsom
- 2dl vörösbor

A marhahúst felvágjuk (felkockázzuk?). Az edényt az olajjal és a hagymával a tűzre rakjuk, megpirítjuk.
Hozzáadjuk a húst és megvárjuk, míg megfehéredik.
Mehet bele a pirospaprika, a kömény és két gerezd zúzott fokhagyma.
Hozzáadjuk a zöldpaprikát, kevés vizet.
Megsózzuk, megborsozzuk.
Kb. fél óra múlva megfűszerezzük egy csipet kakukkfűvel és majorannával.
1 óra elteltével hozzáadjuk a felvágott(felkockázott?) paradicsomot és a vörösbort, és barnásra főzzük.
A legvégén még egyszer beízesítjük zúzott fokhagymával és köménnyel.

---
# ハンガリー　の　煮込み・シュチュー

# Zairyou
- 牛肉（ぎゅうにく）ー　1キロ
- 玉葱（たまねぎ）ー　300g
- 油（あぶら）ー　大さじ4（おおさじ）
- 塩（しょう）・こしょう　ー　少々
- パプリカ粉（こ）ー　大さじ1
- クミン　ー　小さじ1　（こさじ）
- 大蒜（にんにく）　ー　3片（ひら）
- ピーマン　ー　1個（こ）
- タイム 　ー　1つまみ
- マジョラム　ー　1つまみ
- トマト　ー　1個（こ）
- 赤ワイン　ー　2dl

# Tsukurikata
まず、牛肉をカットします、そして　鍋を火にかけて、　油　と　玉ねぎをいれて、　よく　いためます。
次に、肉をいれて、白になって時まで待ってます。
パプリカ　と　クミン　と　３片　大蒜　を　入れます、ピーマン　と　水　少々　を　加えます。
やく30分後、タイム　と　マジョラム　を1つまみ　入れます。
1時間後、角切り　トマト　と　赤ワイン　を　加えて、茶色　に　なるまで　煮込みます。

--- 

# Javítás
shiroku narutoki made machimasu