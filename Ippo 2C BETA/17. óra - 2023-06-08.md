# Új szavak, kifejezések

| romaji             | magyar              |
| ------------------ | ------------------- |
| shinpaigoto        | aggódás             |
| sutoresu ga tamaru | felgyűlik a stressz |
| kaishoubou         | csökkentési mód     |
| yakunitachi        | hasznos             |
| okori              | mérges              |
| kega               | sérülés             |
| narandeimasu       | sorakozik           |


# PDF
![[./assets/pdf/17_ora.pdf]]