# Új szavak, kifejezések

| romaji           | magyar                          |
| ---------------- | ------------------------------- |
| haoru, haorimasu | magára ölt                      |
| kimemasu         | eldönteni                       |
| awaseru          | illeszteni                      |
| himo             | szalag                          |
| musubimasu       | összeköt                        |
| musubi           | masni                           |
| mawasu           | megforgat, megfordít            |
| chiri            | földrajz                        |
| nado             | többek között                   |
| erabu, erabimasu | választani                      |
| kisoku           | szabály                         |
| kibishi(i)       | szigorú                         |
| youi             | előkészület                     |
| higaeri          | egynapos utazás                 |
| taikai           | bajnokság, gyűlés, nagy verseny |
| sanka suru       | részt venni, csatlakozni        |

# Házi / Shukudai
- első jamboard 18. dia, alsó feladat
	5. higaeri
	6. youi
	7. mondai
	8. chuumon?
	9. kyoumi

# PDF
![[jpn/Ippo 2C BETA/assets/pdf/04_ora.pdf]]