| romaji            | magyar                  |
| ----------------- | ----------------------- |
| shusseki          | jelenlét                |
| kakunin suru      | ellenőrizni             |
| aite              | másik fél               |
| bamen             | szituáció               |
| kikikaesu         | visszakérdezni          |
| hayakuchi         | hadaró, gyorsan beszélő |
| nan to iimasu ka? | hogyan mondjuk?         |
| untenmenkyou      | vezetői engedély        |

Érdemes átnézni:
- számlálószavak

# Házi / Shukudai
- keddi jamboard 7. óra: tari-tari gyakorlás
	1. yondari, mitari
	2. nondari, suitari
	3. kiitari, kaitari
	4. tottari, motte kiitari
	5. abiitari, haitari


# PDF
![[./assets/pdf/18_ora.pdf]]