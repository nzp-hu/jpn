# Új szavak, kifejezések

| romaji         | magyar          |
| -------------- | --------------- |
| tazuneru       | meglátogatni    |
| ippai          | sok, teli       |
| ippai tabemasu | rengeteget enni |
| sotsugyou      | ballagás        |
| kashuu         | énekes          |
| sakkyoku suru  | zeneszerzés     |

# Házi / Shukudai
- következő órára iskolával kapcsolatban szókincset gyűjteni

# PDF
![[jpn/Ippo 2C BETA/assets/pdf/01_ora.pdf]]