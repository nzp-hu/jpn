# Új szavak, kifejezések
| kanji | kana  | magyar                     |
| ----- | ----- | -------------------------- |
| 世界史   | せかいし  | világtörténelem (tantárgy) |
| 材料    | ざいりょう | hozzávalók                 |
| 要る物   | いるもの  | szükséges (dolog)          |

# PDF
![[./assets/pdf/16_ora.pdf]]