# Új szavak, kifejezések
| kanji | kana | magyar    |
| ----- | ---- | --------- |
| 縄跳び   | なわとび | ugrókötél |

# Házi / 宿題
- negyedik jb. 13. dia
	1. 自分で自転車が修理できますか。
	2. あの人の名前が思い出せません。
	3. 明日10時ごろこられると思います
	4. 一人で病院へ行けなかったんです。
	5. 10時までに帰れたら、電話をください。
	6. タワポンさんはパソコンが使えないと言いました。

# PDF
![[./assets/pdf/14_ora.pdf]]