# Új szavak, kifejezések

| romaji                | magyar                     |
| --------------------- | -------------------------- |
| ten                   | pont                       |
| futsukayoi            | másnapos                   |
| yopparai              | részeg                     |
| oreru                 | (csont) törés              |
| oremasu               | csont törik el             |
| nodo                  | torok                      |
| mitsukeru/mistukemasu | megtalálni valamit/valakit |
| soushimasu            | "úgy fogok tenni"          |
| nodoame               | torokcukorka               |
| nameru                | (cukorkát) szopogatni      |
| tomaru/tomarimasu     | megszállni valahol         |

# Arról van szó, hogy... / ~んです
Magyarázó szerkezet: *Arról van szó, hogy...*

Képzése:
> ige egyszerű formája + んです
> 
> な melléknév/főnév + な + んです
> 
> い melléknév + んです


# Jobban tennéd, ha... / ~たほうがいいです
Tanács adásához használt: *jobban tennéd, ha...*

# Egyszerű tagadó forma
#todo a táblázatot kimásolni a jamboard-ból

