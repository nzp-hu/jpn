# Új szavak, kifejezések

| romaji               | magyar                            |
| -------------------- | --------------------------------- |
| sugosu / sugoshimasu | eltölteni időt (pl. rendezvényen) |
| kazaru / kazarimasu  | díszíteni                         |
| dokonimo             | sehová                            |
| oriru / orimasu      | leszállni (pl. buszról)           |
| oku / okimasu        | lerakni valahova                  |
| shimeru / shimemasu  | bezár, becsuk                     |
| harau / haraimasu    | fizetni                           |
| oboeru / oboemasu    | emlékezni                         |
| tatsu / tachimasu    | állni                             |
| hairanai             | nem fér be valahova               |
| shuccho suru         | üzleti útra menni                 |
| nakusu / nakushimasu | elveszíteni valamit               |
| kusai                | büdös                             |
| juuyou               | fontos                            |
| shimekiri            | határidő                          |

# Jobban tennéd, ha nem...
> ～ないほうがいいです


# Házi
- 3-as jamboard 15. dia: magyar, masu forma, szótári forma, ta forma, nai forma

| magyar | masu | szótári | ta  | nai |
| ------ | ---- | ------- | --- | --- |
|        |      |         |     |     |

