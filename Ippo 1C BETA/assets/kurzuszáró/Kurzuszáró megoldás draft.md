# 1. Kanjik
1. 休み
2. 行く
3. 行きたいけど
4. 海
5. 見たい
6. 山
7. 行きたい
8. 学校
9. 6月15日
11. 8月31日
13. 休み
14. ６月16日
15. 行きたい
16. 日
17. 金曜日
18. 天気
19. 雨
20. 時間
21. 水
22. 買い物
23. 行かない

# 2. Alkoss 3 mondatot a ～てください  segítségével
1. te wo aratte kudasai - て　を　あらって　ください
2. suwatte kudasai - すわって　ください
3. (chotto) matte kudasai - （ちょっと）まって　ください

# 3. Összekötés
1. 11 ji ni ie wo - demasu
2. botan wo - oshimasu
3. tomodachi ni machi wo - annaishimasu
4. kodomo ni eigo wo - oshiemasu
5. osara wo - araimasu
6. pasokon wo - tsukaimasu
7. eki no mae de tomodachi wo - machimasu

# 4. Írj mondatokat a példákhoz hasonlóan
1. kinou yori kyou no hou ga atsui desu - きのう　より　今日　のほうが　あついです
2. raishuu wa konshuu yori hima desu - 来週　は　今週　より　ひま　です
3. kono sumaatofon wa watashi no pasokon yori takai desu - この　スマートフォン　は　私のパソコン　より　たかい　です
4. anime wa manga yori omoshiroi desu - アニメ　は　まんが　より　おもしろい　です
5. getsuyoubi yori nichiyoubi no hou ga tanoshii desu - 月曜日　より　日曜日　のほうが　たのしい　です

# 5. た vagy ない forma?
1. tomodachi ga matteimasu kara, hayaku itta hou ga iidesuyo - いった
2. sono gyuunyuu wa furui desu kara, nondanai hou ga ii desuyo - のんだない
3. kono kawa ha abunai desu kara, koko de watattanai hou ga ii desuyo - わたったない
     kono kawa ha abunai desu kara, koko de, kono hashi wo watatta hou ga ii desuyo -　この川　は　あぶない　ですから、ここで、 この　はし　を　わたった　ほうがいいですよ。
4. netsu ga arimasu kara, yasunda hou ga ii desu yo - やすんだ
5. ashita no tesuto wa muzukashii desu kara, benkyou shita hou ga ii desu yo - べんきょう　した

# 6. Mit csinálnak a képen? A ～ている forma segítségével írj 5 mondatot

| magyar                    | romaji                                     | kana                                             |
| ------------------------- | ------------------------------------------ | ------------------------------------------------ |
| shinji énekel             | shinji san wa utatteimasu                  | しんじさん　は　うたっています                   |
| keniichi kávét iszik      | kennichi san wa koohi wo nondeimasu        | けんいちさん　は　コーヒ　をのんでいます         |
| erika shinjivel beszélget | erika san wa shinji san to hanashite imasu | えりかさん　は　しんじさん　と　はなして　います |
| tomoko pizzát vág fel?    | tomoko san wa piza wo kitte imasu          | ともこさん　は　ピザ　を　きっています           |
| midori hamburgert eszik   | midori san wa hambaaga wo tabeteimasu      | みどりさん　は　ハンバーガー　を　食べています   |
