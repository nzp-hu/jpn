1. Tudok németül és japánul.
   ドイツ語と日本語ができます。
2. Tamara tud japán TV-t nézni.
   タマラさんは日本のテレビを見ろことができます。
3. Jártál már Japánban?  
   日本へ行ったことがあるますか。
4. Csokievéskor vagyok a legboldogabb. 😊  
   チョコレートを食べているときが一番幸せです
5. A: Az étteremben szabad dohányozni?
   レストランでたばこをすってもいいですか。
   B: Nem, itt nem szabad dohányozni, mert ez egy nemdohányzó hely. A teraszon lehet dohányozni.  
   いいえ、
6. Az egy japán útlevél, az meg egy magyar útlevél. 
   
7. A: Holnap nem megyünk kirándulni?  
   明日旅行に行きませんか。
   B: Holnap lehet, hogy hideg és felhős idő lesz. Talán esni is fog az eső.  
   明日は寒くて、くもりの天気がいるかもしれません。雨もふるかもしれません。
8. Mivel beköszöntött a tavasz, meleg lett. Nyílnak a cseresznyefavirágok.  
   春になりましたので、あついになりました。さくらはさきます。
9. Hőssé szeretnék válni!  
   ヒーローになりたい。
10. Koszos a szobád, ezért azonnal takarítsd ki!  
    
11. Már nagyon hosszú a hajam, ezért kérem vágja kicsit rövidebbre!
    私のかみはながいので、ちょっと短いになってください。
12. A nyári szünetben minden napot szórakoztatóan töltöttem.
    夏休みで毎日は楽しかったです。
13. Úgy döntöttem, hogy m nem megyek buliba, mert nagyon fáradt vagyok.
    とてもつかれたですので、パーテぃーに行くことにしません。
14. Döntsünk úgy, hogy idén Horvátországba megyünk pihenni.  
    今年クロアチアに旅行することにしましょう。
15. Szerénykedem átadni Önnek ezt a Magyarországon vásárolt Tokaji bort.  
    これハンガリーで買ったワインをさしあげます。
16. Hálásan köszönöm Önnek, hogy Tamarának ilyen szép ajándékot adott.  
    タマラにプレセントをくださいますをありがとうごさいます。
17. Tegnap a vendégeimtől kaptam ezt a finom tortát.  
    これおいしいケーキが昨日（きのう）は私のおきゃくさまがいただきました。
18. A: Mi a baj?
    大丈夫ですか。
    B: A héten egy csomó beadandót kell írnom...  
    今週たくさん宿題（しゅくだい）をしなかればなりません。
19. Nem kellene megmutatni a rendőrnek az útlevelet?
    パスポートはお巡りさんに見せなければなりませんか。
20. Ezzel a jeggyel csak és kizárólag a személyvonatokra lehet felszállni!  
    
21. A: Hol vagy most?  
    いま、どこですか。
    B: Most haladok át a Lánchídon.  
    
    A: Hívj fel, amikor leszálltál a villamosról!  
    
22. A kanjikat írva memorizálom  
    
23. A fagyizóig gyalog menjünk.  
    
24. A: Tanárnő, a tesztet mikor írjuk meg?  
    
    B: A tesztet holnap írjuk meg.  
    
25. A: Láttad már A Mandalóri című sorozatot?
    
    B: Még nem. Nem volt rá időm.  
    
26. A: 1 pár zokni mennyibe kerül?  
    
    B: 1 pár zokni kéccá, 10 pár zokni ezeöccá. Minden ezeöccá!  
    
27. A: Nézd azt a helyes japán pasit!  
    
    B: Nagyon menő, de nem tudom, hogy beszél-e magyarul vagy nem. ☹  
    
28. Megkérdeztem, hogy mi lenne jó ajándéknak.  
    
29. Tudod, hogy Yamada hova ment?  
    
30. A: Nyitva van az ajtó! Te nyitottad ki?  
    
    B: Nem! Lehet, hogy betörő van a házban?!
    
