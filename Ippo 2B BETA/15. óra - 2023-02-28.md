# Új szavak, kifejezések

| romaji      | magyar          |
| ----------- | --------------- |
| uwagi       | felső ruházat   |
| shitagi     | alsó ruházat    |
| nugimasu    | levenni (ruhát) |
| kankoukyaku | turista         |

# は partikula, kiemelés

