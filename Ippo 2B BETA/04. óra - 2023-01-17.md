# Új szavak, kifejezések

| romaji                  | magyar                      |
| ----------------------- | --------------------------- |
| haitatsu                | kézbesítés, házhozszállítás |
| haitatsunin             | futár                       |
| dasshutsu geemu         | szabadulószoba (játék)      |
| shussan                 | szülés                      |
| kagakuhakubutsukan      | természettudományi múzeum   |
| mukou                   | túloldal, másik oldal       |
| robii                   | előcsarnok, lobby           |
| gozenchuu               | a délelőtt folyamán         |
| jikokuhyou              | menetrend                   |
| tomeru                  | megállítani, leparkolni     |
| gomi wo suteru/sutemasu | szemetet eldobni            |
| kabuto, herumetto       | sisak                       |
| kabuto wo kaburu        | sisakot viselni             |

# Engedély { megadása, elutasítása }



# Házi / Shukudai
- első jamboard 18. dia alsó: kérdéseket képezni
	1. kaette mo ii desuka?
	2. terebi wo keshite mo ii desuka?
	3. tabako wo sutte mo ii desuka?
	4. mado wo akete mo ii desuka?
- második jamboard 4. dia
	- kabuto wo kabutte ha ikemasen
	- shashin wo totte ha ikemasen
	- inu to itte / haitte ha ikemasen
	- tabete ha ikemasen

---

# PDF
![[./assets/pdf/04_ora.pdf]]