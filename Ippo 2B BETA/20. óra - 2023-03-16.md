# Új szavak, kifejezések

| romaji  | magyar                               |
| ------- | ------------------------------------ |
| taoreru | eldől, elájul                        |
| fuutou  | boríték                              |
| komu    | bezsúfolódik                         |
| koshou  | elromlani (ált. elektronikai eszköz) |


# Házi / Shukudai
- hatodik jamboard 16. dia
	1. kono koppu ha yogorete imasuyo
	2. kono fukuro ha yaburete imasuyo
	3. kono teepurekooda ha kowarete imasuyo
	4. kono koohii ha tsumetaku natte imasuyo
