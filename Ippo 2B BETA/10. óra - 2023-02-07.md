# Új szavak, kifejezések

| romaji             | magyar                  |
| ------------------ | ----------------------- |
| juuyou(na)         | fontos                  |
| taijuu             | testsúly                |
| hakobi, hakobimasu | szállít, cipel          |
| hoomu              | peron                   |
| zasekishiteiken    | helyjegy                |
| kanarazu           | feltétlenül, okvetlenül |
| kyoumi             | érdeklődés              |

# Kell vmit csinálni - ~なければなりません
**Képzése:** az ige ~ない nélküli formája + なければなりません

# Házi / Shukudai
- harmadik jamboard 16. dia: (dobozos) ~nakerebanarimasen
	1. kabura nakereba narimasen
	2. kaka nakereba narimasen
	3. shi nakereba narimasen
	4. tora nakereba narimasen

---

# PDF
![[./assets/pdf/10_ora.pdf]]