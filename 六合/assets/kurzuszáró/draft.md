~3 mondat each

# １。今年のクリスマス
クリスマスイヴで両親と祖母に合いにSopronへ行きたい。Sopronで　従弟たち（いとこたち）や　従弟たち　の　かぞく　も　合いたいです。
休日　に　名付け子（ねづけこ）たち　の　かぞく　を　見に行きたい、子供たちのプレゼントもあげたいです。
上の子（二歳）は 軍用（ぐんよう）のヘリコプターと銃（じゅう）にあげて、そして　下の子　に　噛み応え（かみごたえ）の物　を　あげます。
それぞれ　の　かぞく　は　ちがう　の　町　に　住んでいます　ので、みんなに合いに十分な　時間　が　あるのだおるか？

# 1. Karácsony (hogy töltöd, kinek mit adsz/kitől mit szeretnél kapni, miért)
Karácsonykor/Szenteste meglátogatom a szüleimet, nagymamámat, illetve az unokatestvéreimet és családjaikat.
Az ünnepek alatt szeretném még meglátogatni a két keresztfiam (nadzukeko?) családját is; szeretném elvinni a két gyereknek vásárolt játékot is.
A nagyobbik (2 éves) katonai helikoptert és puskát kap, a kisebbik (fél éves) pedig valami rágcsálhatót.
Mindegyik család más-más városban lakik.. vajon lesz időm mindenkivel találkozni/mindenkit meglátogadni?

# ２。大晦日とお正月
大晦日　は　友達　と　いっしょに　すごします。でも　正確な（せいかくな）計画　が　ありません。
友達の家に合ったり、飲んだり、話す　見込み（みこみ）です。後で、いっしょに　町　に　行きます。
お正月はBudapestに帰ります。電車はにぎやかじゃない といいな。

# 2. Szilveszter, újév (hogy töltöd, kivel)
A Szilvesztert barátaimmal együtt töltöm, de sajnos konkrét terv még nincsen.
Előre láthatólag egyik barátomnál találkozunk, iszogatunk és beszélgetünk, majd valószínűleg bemegyünk a városba.
Újév napján valószínűleg utazok haza Budapestre.
Remélem, hogy nem lesznek sokan a vonaton...

# ３。新年の誓い
todo

# 3. Újévi fogaladom (van-e, mi az, miért)
Újévkor idén is megfogadom, hogy nem fogadok meg semmit.
Ezzel is teljesítve az újévi paradoxont.
Kanpeki desu.

# ４。来年の計画
今年中止（ちゅうし）でしたから、来年に家の台所を刷新（さっしん）したい。
年末前エネルギーやお金が余ると、居間をレノベートします。
そのほかに何もしたくないです。
来年の計画は家は握る（にぎる）と思います。

# 4. Újévi terv (van-e, miért)
Az újévben szeretném megcsinálni az új konyhát otthon - mivel idén az elmaradt.
Ha marad energiám és pénzem, akkor lehet, hogy a nappali padlóját is kicseréltetem.
Ezeken kívül grandiózus terveim nincsenek.
Úgy tűnik, hogy a jövő év terveit dominálja a lakás. :)
