# Új szavak, kifejezések
| kanji      | kana         | magyar                |
| ---------- | ------------ | --------------------- |
| お祝いする | おいわいする | ünnepelni             |
| 飾る       | かざる       | díszíteni             |
| 飾り       | かざり       | díj                   |
| 包む       | つつむ       | becsomagol            |
| 高級       | こうきゅう   | luxus, magas minőségű |
| 雪だるま   | ゆきだるま   | hóember               |

# Házi / Shukudai
- (szorgalmi) hetedik jamboard 8. dia: lefordítani

# PDF
![[./assets/pdf/24_ora.pdf]]