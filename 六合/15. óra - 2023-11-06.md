| kanji        | kana           | magyar                                       |
| ------------ | -------------- | -------------------------------------------- |
| 用法         | ようほう       | használat(i mód)                             |
| 注意         | ちゅうい       | figyelem, figyelni vmire                     |
| 注意点       | ちゅういてん   | fontos pont (amire érdemes figyelni)         |
| 理解する     | りかいする     | megérteni, felfogni vmit                     |
| 受ける       | うける         | kapni, részt venni                           |
| 抑える       | おさえる       | csillapítani, megfékezni, féken tartani vmit |
| 食前         | しょくぜん     | étkezés előtt                                |
| 我慢できない | がまんできない | elviselhetetlen                              |
| 以上         | いじょう       | vmi felett, több mint..                      |
|              |                |                                              |

# Órai munka - 5. jb 2. dia
1. seki wo saeru, ichinichi sankai, shokugo, nomu to nemuku narimasu, untenshinai
2. arerugi, hanamizu, ichinichi ikkai, neru mae ni
3. netsu wo sageru, 38.5, 6jikangurai + aida wo akete, @netsu wo takakute tsurai toki
4. itamidome, i wo mamoru no kusuri to isshoni, gamandokinai

# Házi / Shukudai
- *(szorgalmi)* ötödik jamboard, 4. dia: teimasu
- *(szorgalmi)* ötödik jamboard, 5-6-7-8. dia: közvetlen stílus

# PDF
![[./assets/pdf/15_ora.pdf]]