# Új szavak, kifejezések
| kanji        | kana           | romaji                    |
| ------------ | -------------- | ------------------------- |
|              | ワンピース     | egész alakos ruha (dress) |
|              | くちひげ       | bajusz                    |
|              | あごひげ       | szakáll                   |
| 塗る         | ぬる           | (rá)kenni                 |
|              | ネックレス     | nyaklánc (jövevényszóval) |
| 首飾り       | くびかざり     | nyaklánc                  |
| 模様         | もよう         | minta                     |
| チェック模様 | チェックもよう | kockás minta              |
| 縞模様       | しまもよう     | csíkos minta              |
| 深い         | ふかい         | mély                      |
| 浅い         | あさい         | sekély                    |
| 動く         | うごく         | mozogni                   |
| 動き         | うごき         | mozgás                    |
|              | アリ           | hangya                    |

#todo Ankiba felvinni a ruhaviselés igéket

# Házi / Shukudai
- ötödik jamboard 12. dia: leírást írni az emberekről + egérről
	- ロレナさんはきいろのTシャツをきて、ジーンズをはいている女の人です。
	- ジョンさんはしまもようのシャツをきて、ジーンズをはいている男の人です。
	- サシャさんはピンクのタンクトップをきて、あおいジーンズをはいている女の人です。
	- アーコシュジさんはジンズをはいて、白いくつをはいている男子です
- ötödik jamboard 14. dia: maradékról írni
	- フクロウは目が大きいです。
	- ダックスフントは足が長いです。
	- チｨラノサウルスは腕が短いです。

# PDF
![[./assets/pdf/16_ora.pdf]]