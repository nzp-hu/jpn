# Új szavak, kifejezések
| kanji    | kana           | magyar               |
| -------- | -------------- | -------------------- |
| 残業     | ざんぎょう     | túlóra               |
| 宿題     | しゅくだい     | házi feladat         |
| 注文する | ちゅうもんする | rendelni             |
| 注文     | ちゅうもん     | rendelés             |
| 店内で   | てんないで     | az ütletben, helyben |
| 誓い     | ちかい         | fogadalom            |

# Házi / Shukudai
- hetedik jamboard 7. dia: Adás-kapást feldolgozni magunktól
- **hetedik jamboard 9. dia**
- hetedik jamboard 6. dia: fogalmazást írni (1 hosszú vagy több kicsi) január 8-ig
- *(szorgalmi)* Japán érettségi: https://dload-oktatas.educatio.hu/erettsegi/feladatok_2023tavasz_kozep/k_japan_23maj_fl.pdf
  Hanganyag: https://www.oktatas.hu/bin/content/dload/erettsegi/feladatok_2023tavasz_kozep/k_japan_23maj_fl.mp3
  Megoldókulcs: https://dload-oktatas.educatio.hu/erettsegi/feladatok_2023tavasz_kozep/k_japan_23maj_ut.pdf

# PDF
![[./assets/pdf/23_ora.pdf]]