# Új szavak, kifejezések

| kanji    | kana    | magyar            |
| -------- | ------- | ----------------- |
| 飼います | kaimasu | (állatot) tartani |

# Házi / Shukudai
- gyerekkorban amit szerettem volna ÉS nem szerettem volna csinálni, 3-3 mondatot írni

# PDF
![[./assets/pdf/05_ora.pdf]]