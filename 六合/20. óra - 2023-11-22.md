# Új szavak, kifejezések
| kanji        | kana             | magyar                |
| ------------ | ---------------- | --------------------- |
| 家賃         | やちん           | bérleti díj           |
|              | ていねい（な）   | udvarias              |
| 個人的（な） | こじんてき（な） | személyes, egyéni     |
| 理由         | りゆう           | ok, vmi oka           |
| 謝る         | あやまる         | bocsánatkérés         |
| 言い訳       | いいわけ         | mentség, mentegetőzés |
|              | レジ             | pénztár               |

# ~ので használata
- udvarias beszédmód
- egyéni/személyes ok, bocsánatkéréskor

# Házi / Shukudai
- (*szorgalmi*) [[jelzos_szerkezet_gyakorlo1.png]], [[jelzos_szerkezet_gyakorlo2.png]]
- hatodik jamboard 18. dia felső: lefordítani a mondatokat
	1. A: Jövő héten, amikor Kiótóba mész, busszal mész? Vagy inkább shinkansennel?
	  B: Shinkansennel. ("A shinkansent választom"?)
	2. A: Mit kér inni?
	   B: Sört, legyen szíves.
- hatodik jamboard 18. dia alsó: kitölteni
	1. いつ
	2. ラーメン
	3. 外
	4. タクシー

# PDF
![[./assets/pdf/20_ora.pdf]]