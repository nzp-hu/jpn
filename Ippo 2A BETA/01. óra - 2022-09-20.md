# Új szavak, kifejezések

| romaji       | magyar            |
| ------------ | ----------------- |
| hashiru      | futni             |
| haisha       | fogorvos          |
| youji ga aru | dolga van         |
| hayaku       | gyorsan           |
| yoku         | gyakran/jól       |
| daitai       | majdnem/többnyire |
| mou          | már/még           |
| mou sukoshi  | még egy kicsit    |

1. ane ha hon ga sukina node, yoku toshokan ni ikimasu
2. atarashii ie na node, totemo kirei desu
3. jikan ga nai node, hashitte kudasai
4. ha ga itai node, haisha ni ikimasu
5. takusan benkyou shita node, tesuto ha 100 ten deshita
6. akachan ga neteiru node, koko de asobanai de kudasai


---

# PDF
![[./assets/pdf/01_ora.pdf]]