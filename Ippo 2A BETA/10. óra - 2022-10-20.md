# Új szavak, kifejezések

| romaji           | magyar         |
| ---------------- | -------------- |
| yotei            | terv           |
| (wo) otozureru   | meglátogatni   |
| (wo) houmon suru | meglátogatni   |
| gairaigo         | jövevényszavak |
|                  |                |

# Szándék - ~つもり

**Képzése:** ige szótári alakja + つもり

**Tagadásai:**
- *("tsumori tagadása")* ige + tsumori + wa arimasen
- ige tagadó módban + tsumori desu


# Órai 
1. コンサート　に　いく　つもりです。
2. かぞく　を　ほうもん　する　つもりです。
3. いえ、のうりょくしけん　を　うけません ==うけない==　つもりです。


# Házi / Shukudai
- ==(beküldős)== Negyedik jamboard, 7. dia felső: kifejtős feladat
	1. アメリカ　へ　行く　つもりです。
	2. むすこ　と　ほっかいどう　を　りょこうする　つもりです。
	3. その　小さくて、かるい　カメラ　を　買う　つもりです。
	4. 大学　の　ちかく　すむ　つもりです。
- ==(beküldős)== Negyedik jamboard, 7. dia alsó: eldöntendő kérdések
	1. いいえ、　さんか　しない　つもりです。
	2. ええ、つれて　いく　つもりです。
	3. いいえ、うける　つもり　は　ありません。
	4. ええ、とる　つもりです。
- Negyedik jamboard, 8. dia: jövevényszavakat kitalálni, melyik nyelvből érkeztek


---

# PDF
![[./assets/pdf/10_ora.pdf]]