# 1. Válaszolj a kérdésekre teljes mondatban!
1. ええ、パソコンをつかうことができます。
2. 私はハンガリ語とえい語と日本語ができます。
3. はい、二年ぐらい日本語を勉強しました。
---
1. この週末Győrへ、ともだちへあそびに行きます。
2. ちょっといそがしい日ですので、私はだれも合いたくない。

# 2. Fordítsd le az alábbi mondatokat japánról magyarra!
- A kávét körülbelül 200 évvel ezelőtt a hollandok hozták magukkal Japánba.
- A Meiji/Mejdzsi időszak kezdetéig kevés kávét fogyasztó ember volt, viszont *manapság* egy japán ember egyedül egy év alatt körülbelül 300 csésze kávét iszik meg.
- Amúgy is, van olyan, aki szerint ártalmas a kávé?
- Valójában a kávénak mindenféle/számos jótékony hatása van

# 3. Fordítsd le az alábbi mondatokat magyarról japánra!
1. 週末はショッピングモールへ行ったり、休んだり、えいがをみたり、
   ケーキをたべたりします。
2. クリスマスツリーを買うまえに、かかくを見ます。
3. ことしわたしはよかったですから、私はサンタクロースにチョコレートをもらいます。
4. ダックスフントは足が短いので、かわいいです。
5. クリスマスに大きなテレビがほしいです。

<div style="page-break-before: always;"></div>

# 4. Fogalmazás
２０１７年バルセロナへ、会社の会議（かいぎ）に行きました。ブダペストからひこうきでフランクフルトまで、そしてフランクフルトからバルセロナまで行きました。旅（たび）は3時間ぐらいかかりました。とてもすてきな山々の上にとびました。あとで、ひこうじょうからタクシーで会議（かいぎ）に行きました。会議（かいぎ）の後、観光（かんこう）をしました。都会（とかい）はきれいくて、とても大きかったです。いろいろな建物（たてもの）を見ました。そしてりょこうしゃがたくさんいました。したまちで巨大な（きょだいな）噴水（ふんすい）がいちばんすきました。夜海もみました、これもきれいでした。りょこうはとても楽しかったです。友達といっしょにバルセロナへもう一回行きたいです。

![[yamayama.jpg|200]] ![[yoruumi.jpg|200]]
![[funsui.jpg|403]] 

## Fogalmazás vázlat
- 2017-ben Barcelonába mentem, céges konferenciára.
- Bp-ről Frankfurtba, majd onnan Barcelonába repültem.
- Körülbelül 3 órát tartott az út.
- Gyönyörű hegyek felett repültünk.
- Aztán a reptérről taxival mentem a konferenciára.
- Az előadások után "várost néztem".
- A város szép és nagyon nagy volt.
- Sokféle épületet láttam.
- Emellett sok turista volt.
- Az óriás szökőkutak tetszettek a legjobban a belvárosban
- Este a tengert is megnéztem, az is gyönyörű volt.
- Nagyon jól éreztem magam.
- Szeretnék a barátaimmal együtt még egyszer elmenni.