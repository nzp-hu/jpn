loaded# Új szavak, kifejezések

| romaji                    | magyar                              |
| ------------------------- | ----------------------------------- |
| tsukaikata                | használati mód                      |
| dou omoimasuka?           | mit gondolsz róla? hogyan gondolod? |
| sensei wa dou omoimasuka? | tanár(nő) hogy gondolja?            |
| watashi mo sou omoimasu   | én is így gondolom                  |
| to iimasu                 | azt mondta, hogy...                 |
| koutsuu                   | közlekedés                          |

# Órai munka
>watashi ha yuube kaerimichi ni kuroi neko ni obiemashita
>tegnap este hazafelé megijesztett egy fekete macska


# Házi / Shukudai
- (előző órait megcsinálni, beküldeni)
- (==beküldős==) Harmadik jamboard, 15. dia, 3-as feladat + lefordítani a mondatokat magyarra
	5. あした　あめ　が　==ふる==　と　おもいます。
	   Holnap szerintem esni fog az eső.
	6. この　シャツ　は　デザイン　が　==いい==　と　おもいます。
	   Szerintem annak az  az ing jól néz ki *("jó az ing dizájnja")*
	7. たなかせんせい　は　しんせつな　==せんせい　だ==　と　おもいます
	   Szerintem Tanaka tanár úr egy jószívű tanár.
- (szorgalmi) Harmadik jamboard, 17. dia: mondatokat írni a melléknevekhez

---

# PDF
![[./assets/pdf/08_ora.pdf]]