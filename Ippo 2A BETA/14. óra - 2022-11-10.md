# Új szavak, kifejezések

| romaji         | magyar         |
| -------------- | -------------- |
| tsugou         | körülmény(ek)  |
| moushikomi     | jelentkezés    |
| nouryokushiken | JLPT           |
| shushou        | miniszter      |
| nyuuin suru    | kórházba kerül |
| kenkou         | egészség       |
| rikonshimasu   | elválni        |
| kotaeru        | válaszolni     |
| umareru        | megszületni    |
| kanazuchi      | kalapács       |
| kanemochi      | gazdag         |

# Órai munka - 5. jamboard 3. dia
1. kodomo ga umarete, ureshii desu
2. kanojo kara tegami ga ~~kinakute~~ konakute, kanashii desu
3. jishin no nyuusu wo kiite, bikkurimashita
4. supiichi ga jouzu ni dekinakute, gakkarishimashita

# Órai munka - játék
1. első
	- alany: shushou (miniszter)
	- eszköz: kanazuchi (kalapács)
2. második
	- jelző: kanemochi (gazdag)
	- társ: kaisha no juukyouin
	- állítmány: mimasu


# Házi / Shukudai
- (==beküldős==) [ötödik jamboard](https://jamboard.google.com/d/1LIb1LKKgOEC8Ywo6-T6zAE9AP0Cuj-Mp23BIFFGOwws/viewer?f=3) 4. dia: te forma, majd következményt írni (a kérdésből)
	1. いいえ、たかくて、買えませんでした。
	2. いいえ、ふくざつで、わかりません。
	3. いいえ、車のおとがうるさくて、あまりねられません。
	4. いいえ、風で、さんかできませんでした。
- (romaji)
	1. iie, takakute, kaemasen deshita
	2. iie, fukuzatsude, wakarimasen
	3. iie, kuruma no oto ga urusakute, amari neraremasen
	4. iie, kaze de, sankadekimasen deshita


---

# PDF
![[./assets/pdf/14_ora.pdf]]