# Új szavak, kifejezések
| kanji | kana | magyar  |
| ----- | ---- | ------- |
| 倍     | ばい   | duplája |
| 孫     | まご   | unoka   |

# Házi / 宿題
- első doc 51. dia
	1. 父が私にもっと勉強するように言いました。
	2. つまが(私に)ケーキを買ってくるようにたのみます。
	3. 医者が(私に)お酒をやめたほうがいいように言いました。
	4. 母が早くうちへ帰るようにちゅういしました。
- szorgalmi: japán közmondást keresni; megmagyarázni - vagy magyar közmondást japánul elmagyarázni

# PDF
![[./assets/pdf/10_ora.pdf]]
