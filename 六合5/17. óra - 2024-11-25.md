# Új szavak, kifejezések
| kanji | kana   | magyar                       |
| ----- | ------ | ---------------------------- |
| 音量    | おんりょう  | hangerő                      |
|       | すべりにくい | nehezen csúszik, nem-csúszós |
|       | すべりやすい | könnyen csúszik, csúszós     |
| 昼間    | ひるま    | nap közben                   |
| 新型    | しんがた   | új típusú                    |
| 年齢    | ねんれい   | életkor                      |

# Házi / 宿題
- második doc 21. dia
	1. yuubinbangou
	2. 科学技術(かがくぎじゅつ)を作ること
- második doc 22. dia: alulról beírni a szavakat

# PDF
![[./assets/pdf/17_ora.pdf]]
