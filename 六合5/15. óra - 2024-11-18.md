# Új szavak, kifejezések
| kanji | kana | magyar               |
| ----- | ---- | -------------------- |
| 地域    | ちいき  | terület              |
| 変える   | かえる  | módosít, változik    |
| 味付け   | あじつけ | ízesítés, fűszerezés |

# Házi / 宿題
- második doc 8. dia alsó: messenger csoportban lévő hangok alapján
	1. nakutekamaimasen (nakutemo ii)
	2. (nakutemo ii) nakutemokamaimasen
	3. nakerebanarimasen
	4. (nakutemo ii) nakutemokamaimasen
	5. nakute kamaimasen
- második doc 9-10. dia: ugyanaz a feladat, csak kétszer van a szódoboz
	1. hara~~i~~==wa==nakutemoii
	2. kakanakutemoii
	3. shinpaishinakutemoii
	4. ikanakutemoii
	5. okinakutemoii

# PDF
![[./assets/pdf/15_ora.pdf]]
