# Új szavak, kifejezések
| romaji       | magyar               |
| ------------ | -------------------- |
| jisa         | időeltolódás         |
| gozen        | délelőtt             |
| gogo         | délután              |
| hiru         | dél                  |
| reiji        | éjfél                |
| nan youbi    | milyen nap van?      |
| sumire       | ibolya               |
| doubutsuen   | állatkert            |
| depaato      | áruház               |
| yuuenchi     | vidámpark            |
| yuri         | liliom               |
| kissaten     | kávézó               |
| tokoya       | (férfi) fodrászat    |
| eigyoujikan  | nyitvatartási idő    |
| heijitsu     | hétköznap            |
| ranchi taimu | ebédidő              |
| maru         | igaz                 |
| batsu        | hamis                |
| bijutsukan   | szépművészeti múzeum |
| mou          | már, még             |
| kimasu       | jönni                |

# Hét napjai
| romaji                    | magyar                    |
| ------------------------- | ------------------------- |
| getsuyoubi                | hétfő                     |
| kayoubi                   | kedd                      |
| suiyoubi                  | szerda                    |
| mokuyoubi                 | csütörtök                 |
| kinyoubi                  | péntek                    |
| doyoubi                   | szombat                   |
| nichiyoubi                | vasárnap                  |
| Kyou wa nan youbi desuka? | Ma milyen nap van?        |
| Kyou wa kayoubi desu.     | Ma kedd van.              |
| yasumi                    | szünet, szünnap, pihenés  |
| yasumi no hi              | szünnap                   |
| shuumatsu                 | hétvége                   |
| donichi                   | szombat&vasárnap (szleng) |

A naptári hét általában vasárnappal kezdődik, de emberenként változó.

# Nyitvatartás / -tól, -ig
| romaji                           | magyar                                      |
| -------------------------------- | ------------------------------------------- |
| nan ji kara nan ji made desu ka? | mettől meddig van nyitva?                   |
| ... kara ... made desu.          | ...-től ...-ig van nyitva.                  |
| yasumi wa nan youbi desuka?      | melyik nap van zárva? melyik nap a szünnap? |

# Házi
- Az 5. jamboard 20. diája ([[14_ora.pdf#page=3]]) - használjuk a gogo és gozen szavakat is!

| óra:perc | romaji                      |
| -------- | --------------------------- |
| 01:01    | gozen ichiji ippun          |
| 11:06    | gozen juuichiji roppun      |
| 21:15    | gogo kuji juugofun          |
| 11:40    | gozen juuichiji yonjuppun   |
| 12:02    | gogo juuniji nifun          |
| 23:07    | gogo nijuusanji nanafun     |
| 16:20    | gogo juurokuji nijuppun     |
| 01:45    | gozen ichiji yonjuugofun    |
| 22:03    | gogo juuji sanpun           |
| 19:08    | gogo shichiji happun        |
| 15:25    | gogo sanji nijuugofun       |
| 21:50    | gogo kuji gojuppun          |
| 14:04    | gogo niji yonpun            |
| 18:09    | gogo rokuji kyuufun         |
| 01:30    | gozen ichiji han            |
| 01:55    | gozen ichiji gojuugofun     |
| 13:05    | gogo ichiji gofun           |
| 17:10    | gogo goji juppun            |
| 11:35    | gozen juuichiji sanjuugofun |

- A 6. jamboard 7. diája [[14_ora.pdf#page=10]] - nyitvatartásokat megírni, gogo/gozen legyen benne
	1. oosaka depaato wa gozen juuji han kara gogo shichiji han made desu. yasumi wa mokuyoubi desu.
	2. midori toshokan wa gozen kuji kara gogo rokuji made desu. yasumi wa getsuyoubi to kinyoubi desu.
	3. appuru ginkou wa gozen kuji kara gogo sanji made desu. yasumi wa doyoubi to nichiyoubi desu. (yasumi wa shuumatsu desu.)
	4. sakura byouin wa gozen kuji han kara gogo hachiji han made desu. yasumi wa nichiyoubi desu.


# PDF
![[14_ora.pdf]]