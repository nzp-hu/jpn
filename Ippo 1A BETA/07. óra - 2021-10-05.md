# Új szavak, kifejezések
| romaji     | magyar     |
| ---------- | ---------- |
| yoru       | este       |
| tori       | madár      |
| ki         | fa         |
| akachan    | kisbaba    |
| hana       | virág      |
| michi      | út, utca   |
| beddo      | ágy        |
| doa (door) | ajtó       |
| hako       | doboz      |
| takushii   | taxi       |
| hito       | ember      |
| ningen     | emberi faj |


# Létezés
- **imasu:** élő, mozgó élőlényekre (emberek, állatok, mikrobák)
- **arimasu:** élettelen dolgokra, tárgyakra, *növényekre*, testrészekre, alkatrészekre

## Tagadás
- imasu --> imasen
- arimasu --> arimasen

## Példák
*helyszín* ni *dolog* ga imasu

| romaji                     | magyar              |
| -------------------------- | ------------------- |
| koko ni neko *ga* imasu    | Itt van egy macska  |
| soko ni kasa *ga* arimasu  | Ott egy esernyő van |
| koko ni neko *wa* imasen   | Itt nincs macska    |
| soko ni kasa *wa* arimasen | Ott nincs esernyő   |

# Házi
- Jamboard 10. dia - mindegyik típusból három-három mondatot írni (kérdés-válasz párok), hiraganával leírva
	1. ie ni terebi ga arimasu
	2. niwa ni neko ga imasu
	3. michi ni jitensha wa arimasen
	1. ie ni tokei ga arimasuka? hai, arimasu.
	2. niwa ni hana ga arimasuka? iie, arimasen
	3. michi ni hito ga imasuka? hai, imasu.
	1. ie ni nani ga arimasuka? reizouko ga arimasu.
	2. niwa ni nani ga imasuka? neko ga imasu.
	3. michi ni dare ga imasuka? Gaaboru ga imasu.


# PDF
![[07_ora.pdf]]