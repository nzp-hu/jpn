# Új szavak, kifejezések
| romaji           | magyar              |
| ---------------- | ------------------- |
| himawari         | napraforgó          |
| nuigurumi        | plüssállat          |
| yakan            | teáskanna           |
| usagi            | nyúl                |
| rousoku          | gyertya             |
| ichigo           | eper                |
| onigiris         | rizsgolyó           |
| tentoumushi      | katicabogár         |
| hasami           | olló                |
| tsumiki          | építőkocka          |
| mawaru           | forogni             |
| mushi            | bogár               |
| kusuri           | gyógyszer           |
| tabako           | dohány(áru)         |
| abunai           | vigyázat, veszélyes |
| haiteha ikemasen | belépni tilos       |
| kaeru            | béka                |
| toshokan         | könyvtár            |
| onnanoko         | kislány             |
| shinbun          | újság               |
| booru            | labda               |

# Határozatlan névmások
| romaji  | magyar  |
| ------- | ------- |
| nani+ka | valami  |
| dare+ka | valaki  |
| doko+ka | valahol |
| nani+mo | semmi   |
| dare+mo | senki   |
| doko+mo | sehol   |

# Helyhatározók
határozó + *ni*

| romaji | magyar                    |
| ------ | ------------------------- |
| ue     | felett, rajta (valamin)   |
| shita  | alatt                     |
| naka   | benne                     |
| soto   | kint                      |
| mae    | előtt                     |
| ushiro | mögött                    |
| migi   | valami jobbján            |
| hidari | valami balján             |
| aida   | között                    |
| yoko   | oldalán, oldalt, (fal)-on |
| tonari | szomszédos                |
| soda   | mellett, közelében        |

isu no ue ni neko ga imasu - a széken van egy macska
mado no soto ni onnanoko ga imasu - az ablakban van egy kislány (náluk "ablakon kívül")

# Házi
- a [4. jamboard](https://jamboard.google.com/d/1UCvOh7sIM5bFeK9-Thp7RfrRnhE3DPsdtLyNpWLakbM/viewer?f=0) 1. diája
	1. kaban no naka ni shinbun ga arimasu
	2. teeburu no shita ni booru ga arimasu
	3. ki no ue ni tori ga imasu
	4. kaban no ushiro ni denwa ga arimasu
	5. mado no soto ni saru ga imasu


# PDF
![[jpn/Ippo 1A BETA/assets/pdf/08_ora.pdf]]