# Új szavak, kifejezések
| romaji      | magyar            |
| ----------- | ----------------- |
| hachimitsu  | méz               |
| nekutai     | nyakkendő         |
| koohi       | kávé              |
| mama        | anya              |
| nezumi      | egér              |
| kame        | teknős            |
| yotto       | jacht             |
| hottokeeki  | palacsinta        |
| geemu       | (pc/konzol) játék |
| gamu        | rágógumi          |
| gumi        | gumicukor         |
| gomu        | befőttes gumi     |
| hocikisu    | tűzőgép           |
| minisukaato | miniszoknya       |
| (o)tanjoubi | születésnap       |
| eigakan     | mozi              |
| puuru       | medence, uszoda   |
| deeto       | randi             |
| jugyo       | tanóra            |
| shuu        | hét               |

# Hónapok
| romaji       | magyar     |
| ------------ | ---------- |
| ichigatsu    | január     |
| nigatsu      | február    |
| sangatsu     | március    |
| shigatsu     | április    |
| gogatsu      | május      |
| rokugatsu    | június     |
| shichigatsu  | július     |
| hachigatsu   | augusztus  |
| kugatsu      | szeptember |
| juugatsu     | október    |
| juuichigatsu | november   |
| juunigatsu   | december   |

# Dátumok
| romaji          | magyar           |
| --------------- | ---------------- |
| nangatsu        | melyik hónap?    |
| nannichi        | hányadika?       |
| tsuitachi       | elseje           |
| futsuka         | másodika         |
| mikka           | harmadika        |
| yokka           | negyedike        |
| itsuka          | ötödike          |
| moika           | hatodika         |
| nanoka          | hetedike         |
| youka           | nyolcadika       |
| kokonoka        | kilencedike      |
| tooka           | tizedike         |
| juuichinichi    | tizenegyedike    |
| juuninichi      | tizenkettedike   |
| ..              | ..               |
| juuyokka        | tizennegyedike   |
| ..              | ..               |
| juukunichi      | tizenkilencedike |
| yotsuka         | huszadika        |
| nijuuichinichi  | huszonegyedike   |
| ..              | ..               |
| nijuuyokka      | huszonnegyedike  |
| ..              | ..               |
| sanjuuichinichi | harmincegyedike  |

Elsejétől tizedikéig nagyjából a számlálószavaknak megfelelően hívják őket.

# Születésnap
| romaji                               | magyar                                                |
| ------------------------------------ | ----------------------------------------------------- |
| tanjoubi wa nangatsu nannichi desuka | mikor van (melyik hónap, melyik nap) a születésnapod? |
| tanjoubi wa itsu desuka?             | mikor van a születésnapod?                            |

# Gyakoriság
| romaji    | magyar  |
| --------- | ------- |
| shuu ni   | hetente |
| isshuukan | a héten |

```
~ni ~kai
```

isshuukan ==ni== san==kai== gakkou ni ikimasu

# Házi
- új katakanák
- 5db születésnapot felírni (családtagok, barátok) kérdés-válasz formában
	1. tanjoubi wa itsu desuka? nigatsu nijuusannichi desu.
	2. okaasan no tanjoubi wa itsu desuka? ichigatsu touka desu.
	3. otousan no tanjoubi wa itsu desuka? juugatsu juurokunichi desu.
	4. István no tanjoubi wa nangatsu nannichi desuka? juuichigatsu nijuugonichi desu.
	5. Gergő no tanjoubi wa itsu desuka? juugatsu juuyokka desu.
- jamboard 20. diáját ([PDF 8. dia](./assets/pdf/18_ora.pdf#page=8)) befejezni
	3. anata no tanjoubi wa itsu desuka? nigatsu nijuusannichi desu.
	4. anata wa kaisha ni nan de kimasuka? toramu de ikimasu.
	5. uchi kara kaisha made donokurai desuka? toramu de sanjuppun desu.
- leírni, hogy egy héten hányszor
	1. jársz dolgozni: shuu ni gokai kaisha he ikimasu
	2. mész boltba: shuu ni nikai omise ni ikimasu
	3. kono kurasu ni shuu ni nikai ikimasu
	4. mainichi ni nikai tabemasu.
	5. mainichi ni gokai koohii o nomimasu


# PDF
![[18_ora.pdf]]