# Új szavak, kifejezések
| kanji   | kana       | magyar                 |
| ------- | ---------- | ---------------------- |
| 禁煙      | きんえん       | tilos a dohányzás      |
| 立ち入り禁止  | たちいるきんし    | belépni tilos          |
| 何と読みますか | なん　と　よみますか | hogyan kell kiolvasni? |

# ~と言っていました - mástól hallott infót átadni
Képzése:  szótári alak + と言っていた

# Órai munka
1. buchou ha amerika he shucchou suru to iimashita
2. satou-san ha ekimae no restoran ha oishii to itteimashita
3. asa no terebi de gogo ha ii tenki ni naru to itteimashita

# Házi / 宿題
- harmadik jamboard 4. dia
	1. kotaemashita
	2. torimashita
	3. tarimasen
	4. tetsudaimashita
	5. higaeri
	6. youi
	7. mondai
	8. chuumon
	9.  kyoumi

# PDF
![[./assets/pdf/09_ora.pdf]]