# Új szavak, kifejezések
| kanji  | kana      | magyar                   |
| ------ | --------- | ------------------------ |
| 偉い     | えらい       | tiszteletre méltó        |
| 観覧車    | かんらんしゃ    | óriáskerék               |
| ゴロゴロする | ごろごろする    | lazítani, henyélni       |
| 記録を破る  | きろく　を　やぶる | rekordot dönteni         |
| 横      | よこ        | függőleges dolog oldalán |
| 伝統     | でんとう      | hagyomány                |
| 伝統的な   | でんとうてきな   | hagyományos              |
| 引き出し   | ひきだし      | fiók                     |
| 連絡しり   | れんらくする    | értesíteni               |
| 家賃     | やちん       | bérleti díj              |

# Házi / 宿題
- második jamboard 8. dia alsó
	1. mou kazatte arimasu
	2. mada kaite arimasen
	3. mou tsukutte arimasu
	4. mou irete arimasu
- második jamboard 9. dia
	3. b
	4. ~~b~~ a
	5. b
	6. c
	7. a
	8. a

# PDF
![[./assets/pdf/06_ora.pdf]]