# Új szavak, kifejezések
| kanji | kana | magyar |
| ----- | ---- | ------ |
|       |      |        |

# Házi / 宿題
- első jamboard 4. dia: partikulák alapján a megfelelő alakot kitölteni
	1. shimemasu
	2. hairimasu
	3. tsukemasu
	4. wakimasu
	5. dashimasu
	6. kiemasu
	7. kowaremasu
	8. yogoshimasu
	9. ochimasu

# PDF
![[./assets/pdf/01_ora.pdf]]