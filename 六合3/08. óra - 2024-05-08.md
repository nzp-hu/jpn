# Új szavak, kifejezések
| kanji | kana | magyar |
| ----- | ---- | ------ |
|       |      |        |

# Házi / 宿題
- második jamboard 18. dia
	1. keredomo
	2. desukara
	3. jya
	4. soreni
	5. tatoeba
	6. desukara
	7. sorekara
	8. sorede, soreni
	9. soreni, sorede

# PDF
![[./assets/pdf/08_ora.pdf]]