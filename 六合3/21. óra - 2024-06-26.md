# Új szavak, kifejezések
| kanji | kana | magyar |
| ----- | ---- | ------ |
|       |      |        |

# Házi / 宿題
- hatodik jamboard 10. dia
	1. ~~nansai desuka~~
	2. hiroi desune
	3. mitakoto ga arimasen
	4. oboerarenaindesu
	5. doko no kuni no desuka
	6. totemo suki

# PDF
![[./assets/pdf/21_ora.pdf]]