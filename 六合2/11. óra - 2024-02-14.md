# Új szavak, kifejezések
| kanji | kana | magyar |
| ---- | ---- | ---- |
| 葬式 | そうしき | temetés |

# Házi / Shukudai
- harmadik jamboard: maradék három mondatra írni
	- kodomo no toki, gakkou wo saborimashita
	- 子供の時、学校をサボりました。
	- lottó wo ataru toki, nakimasu
	- lottó を当たる時、なきます。
	- puresento wo kureru toki, kandou shimasu
	- プレセントをくれる時、感動します。

# PDF
![[./assets/pdf/11_ora.pdf]]